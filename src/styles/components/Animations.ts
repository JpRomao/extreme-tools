import { keyframes } from 'styled-components';

export const FadeIn = keyframes`
  from{
    transform: translateY(-45px);
    opacity: 0;
  }
  to {
    transform: translateY(0px);
    opacity: 1;
  }
`;

export const Appear = keyframes`
  from{
    transform: scale(0);
  }
  to{
    transform: scale(1);
  }
`;

export const FadeX = keyframes`
  from{
    transform: translateX(-50px);
    opacity: 0;
  }
  to {
    transform: translateX(0px);
    opacity: 1;
  }
`;
