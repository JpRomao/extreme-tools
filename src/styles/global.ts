import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  :root {
    --white: #ffffff;
    --black: #000000;
    --background: #fffffff6;
    --background-button: #000000dc;
    --gray-line: #ddddee;
  }

  html {
    scroll-behavior: smooth;
  }

  @media (min-width: 1366px) {
    html {
      font-size: 68.75%;
    }
  }

  @media (min-width: 720px) {
    html {
      font-size: 62.5%;
    }
  }

  @media (min-width: 300px) {
    html {
      font-size: 60%;
    }
  }

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    background: var(--background);

    padding: 1.2rem 0;
    color: var(--text);

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    overflow: scroll;
    height: 100%;
    width: 100%;

    footer {
      color: var(--white);
    }
  }

  body, input, textarea, button {
    font: 400 1.4rem "Inter", sans-serif;
  }

  a, button {
    cursor: pointer;
    background: none;
    transition: background-color 1s;
  }

  a {
    color: var(--white);
    text-decoration: none;
  }

  ::-webkit-scrollbar {
    width: 10px;
  }

  ::-webkit-scrollbar-thumb {
    background: var(--black);
    border-radius: 14px;
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.2);
  }

  ::-webkit-scrollbar-track {
    background: #d2d2d221;
  }
`;
