import Head from 'next/head';
import type { AppProps /*, AppContext */ } from 'next/app';

import GlobalStyle from '@styles/global';
import Header from '@components/Header/header';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Extreme Tools</title>
      </Head>
      <Header />
      <Component {...pageProps} />
      <GlobalStyle />
    </>
  );
}

export default MyApp;
