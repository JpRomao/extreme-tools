import { HeaderContainer } from '@styles/components/header';
import Image from 'next/image';

import logo from '@public/images/logo.svg';

interface HeaderProps {
  pageTitle?: string;
}

const Header: React.FC<HeaderProps> = ({ pageTitle }) => {
  return (
    <HeaderContainer>
      <div>
        <Image src={logo} alt="Vercel" />
      </div>
      {pageTitle && <h1>{pageTitle}</h1>}
      {/* <nav></nav> <--------- navbar */}
    </HeaderContainer>
  );
};

export default Header;
