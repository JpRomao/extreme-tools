module.exports = {
  reactStrictMode: true,
  env: {
    FRONTEND_URL: process.env.FRONTEND_URL,
  },
};
